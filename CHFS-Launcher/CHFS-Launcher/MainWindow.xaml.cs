﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using Newtonsoft.Json;
using MessageBox = System.Windows.MessageBox;

namespace CHFS_Launcher
{
	/// <summary>
	/// MainWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MainWindow : Window
	{
		private string settings, chfs;
		private StringBuilder cout;
		private Setting s;
		private Process p;

		public MainWindow()
		{
			InitializeComponent();
			Init();
		}

		private void Init()
		{
			chfs = (Environment.CurrentDirectory + "\\chfs.exe");
			settings = (Environment.CurrentDirectory + "\\settings.json");
			cout = new StringBuilder();
			s = new Setting();
			p = new Process();
			if (File.Exists(chfs))
			{
				if (File.Exists(settings))
				{
					try
					{
						string json = File.ReadAllText(settings);
						s = JsonConvert.DeserializeObject<Setting>(json);
					}
					catch (Exception e)
					{
						MessageBox.Show(e.StackTrace);
						InitJson();
					}
				}
				else
				{
					InitJson();
				}

				if (s == null)
				{
					InitJson();
				}
				txt_path.Text = s.Path;
				txt_port.Text = s.Port;
				ckb_auto.IsChecked = s.Auto;
			}
			else
			{
				MessageBox.Show("Please put this program into the same folder of chfs.exe");
				Environment.Exit(0);
			}

			p.StartInfo.FileName = chfs;
			p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
			p.StartInfo.CreateNoWindow = true;
			p.StartInfo.RedirectStandardOutput = true;
			p.StartInfo.RedirectStandardError = true;
			p.StartInfo.StandardOutputEncoding = Encoding.UTF8;
			p.StartInfo.StandardErrorEncoding = Encoding.UTF8;
			p.StartInfo.UseShellExecute = false;
			p.EnableRaisingEvents = true;
			p.Exited += ExitEvent;
			p.OutputDataReceived += OutputEvent;
			p.ErrorDataReceived += ErrorEvent;

			if (s.Auto)
			{
				Start();
			}
		}

		private void InitJson()
		{
			txt_path.Text = Environment.CurrentDirectory;
			txt_port.Text = "80";
			ckb_auto.IsChecked = false;

			SaveJson();
			Init();
		}

		private void SaveJson()
		{
			s.Path = txt_path.Text;
			s.Port = txt_port.Text;
			if (ckb_auto.IsChecked.HasValue)
			{
				s.Auto = ckb_auto.IsChecked.Value;
			}
			else
			{
				s.Auto = false;
			}

			try
			{
//				File.WriteAllText(settings, JsonConvert.SerializeObject(s));
//				JsonSerializer js=new JsonSerializer()
//				{
//					Formatting = Formatting.Indented,
//					NullValueHandling = NullValueHandling.Ignore
//				};
				File.WriteAllText(settings, JsonConvert.SerializeObject(value: s, formatting: Formatting.Indented));
			}
			catch (Exception e)
			{
				MessageBox.Show(e.StackTrace);
				throw;
			}
		}

		private void ErrorEvent(object sender, DataReceivedEventArgs e)
		{
			if (!string.IsNullOrEmpty(e.Data))
			{
				MessageBox.Show(e.Data.Trim(), "Error!");
			}
		}

		private void OutputEvent(object sender, DataReceivedEventArgs e)
		{
			if (!string.IsNullOrEmpty(e.Data))
			{
				cout.AppendLine(e.Data.Trim());
			}
		}

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			SaveJson();
			Stop();
			p.Close();
		}

		private void btn_browse_Click(object sender, RoutedEventArgs e)
		{
			FolderBrowserDialog fbd = new FolderBrowserDialog();
			fbd.ShowNewFolderButton = true;

			DialogResult dr = fbd.ShowDialog();

			if (dr == System.Windows.Forms.DialogResult.OK)
			{
				txt_path.Text = fbd.SelectedPath.Trim();
			}
		}

		private void btn_st_Click(object sender, RoutedEventArgs e)
		{
			Start();
		}

		private void btn_sp_Click(object sender, RoutedEventArgs e)
		{
			Stop();
		}

		private void btn_soh_Click(object sender, RoutedEventArgs e)
		{
		}

		private void Start()
		{
			Dispatcher.BeginInvoke(new Action(delegate
			{
				btn_browse.IsEnabled = false;
				btn_st.IsEnabled = false;
				btn_sp.IsEnabled = true;
				txt_port.IsEnabled = false;
				ckb_auto.IsEnabled = false;
			}));

			SaveJson();

			p.StartInfo.Arguments = "--path=\"" + s.Path + "\" --port=" + s.Port;

#if DEBUG 
			p.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
			p.StartInfo.CreateNoWindow = false;
//			p.StartInfo.FileName = "cmd.exe";
//			p.StartInfo.Arguments = "/K chfs.exe --path=\"" + s.Path + "\" --port=" + s.Port;
#endif

//			t = new Thread(() =>
//			{
//				p.Start();
//				p.WaitForExit();
//			});
//			t.Start();

			cout.Clear();
			p.Start();
			p.BeginOutputReadLine();
			p.BeginErrorReadLine();
		}

		private void Stop()
		{
			RstBtn();

			try
			{
				p.Kill();
				p.CancelOutputRead();
				p.CancelErrorRead();
			}
			catch (Exception)
			{
				// ignored
			}
		}

		private void ExitEvent(object sender, EventArgs e)
		{
			RstBtn();
			p.CancelOutputRead();
			p.CancelErrorRead();

#if DEBUG
			MessageBox.Show(cout.ToString());
#endif
		}

		private void RstBtn()
		{
			Dispatcher.BeginInvoke(new Action(delegate
			{
				btn_browse.IsEnabled = true;
				btn_st.IsEnabled = true;
				btn_sp.IsEnabled = false;
				txt_port.IsEnabled = true;
				ckb_auto.IsEnabled = true;
			}));
		}
	}

	class Setting
	{
		public string Path { get; set; }
		public string Port { get; set; }
		public bool Auto { get; set; }
	}
}